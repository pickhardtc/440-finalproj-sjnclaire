import java.util.Scanner;
import java.io.File;
import java.util.ArrayList;
import java.io.*;
import java.util.*;

//Main method to run the scheduler
public class ScheduleMain{

  //variable declarations
  public static ArrayList<Process> processes = new ArrayList<Process>();
  public static Queue<Process> pri1 = new LinkedList<Process>();
  public static Queue<Process> pri2 = new LinkedList<Process>();
  public static Queue<Process> pri3 = new LinkedList<Process>();
  public static Queue<Process> pri4 = new LinkedList<Process>();
  public static Queue<Process> pri5 = new LinkedList<Process>();
  public static Queue<Process> pri6 = new LinkedList<Process>();
  public static Queue<Process> pri7 = new LinkedList<Process>();



//  Queue<Process> pri2 = new LinkedList<Process>();

  public static void main(String []args) throws FileNotFoundException{
    //welcome user to process sorter
    System.out.println("Welcome to the MULTIPLE QUEUE SORTING MACHINEEEEE!!!");
    //read information from csv
    readCSV();
    //sort processes in the csv into queues
    sortProc();
    //choose algorithm to run the multiple queues
    chooseAlg();
    //run the algorithm and show which proceeses are run in what order
  }


  //method that reads in the CSV of the processes
  public static void readCSV(){
    //introduce scanner and variables
    Scanner scan = new Scanner(System.in);
    //import information from csv file
    System.out.print("What is the name of the file with process to read in?: ");
    File file = new File(scan.nextLine());
    //put into into Array of processes
    String line;
    Scanner sepString;
    String[] hold;
    try{ //try block to catch any potential errors and handle them
      Scanner scan2 = new Scanner(file);
      while(scan2.hasNextLine()){
        line = scan2.nextLine();
        //System.out.println(line); //test
        hold = line.split(",\\s*");
        processes.add(new Process(hold[0], hold[1]));
      }
    } catch (FileNotFoundException ex){
      System.out.println("Could not find file");
    }
    processes.toString();
}

  //method to sort the processes into their respective types
  public static void sortProc(){
    int counter = 0;
    Process p;
    while(counter < processes.size()){
      p = processes.get(counter);
      System.out.println(p.getId()+"   "+p.getPT());

      //sorts the processes into their types
      if(p.getPT().equals("SYS")){
        pri1.add(p);
      } else if (p.getPT().equals("BACKGROUND")){
        pri2.add(p);
      } else if (p.getPT().equals("INTERACTIVE")){
        pri3.add(p);
      } else if (p.getPT().equals("EDIT")){
        pri4.add(p);
      } else if (p.getPT().equals("BATCH")){
        pri5.add(p);
      } else if (p.getPT().equals("USER")){
        pri6.add(p);
      } else if (p.getPT().equals("NONE")){
        pri7.add(p);
      } else {
        System.out.println("Something went very, very wrong");
      }
      counter++;
    }

  }

  //method that chooses the algorithm used in each queue to prioritize processes
  public static void chooseAlg(){
    System.out.println("Choose an algorithm! \n\t1. priority\n\t2. split\n\t3. order");
    Scanner ans = new Scanner(System.in);
    String answer = ans.next();
    if(answer.equals("1")){
      System.out.println("Processes will be run by priority!");
      priorityAlg();
    } else if (answer.equals("2")){
      System.out.println("Processes will be run on a Round Robin Schedule");
      rrAlg();
    } else if (answer.equals("3")){
      System.out.println("Processes will be run in First Come First Served Order");
      orderAlg();
    } else {
      System.out.println("You have to choose a valid option! ");
      chooseAlg();
    }

  }

  //method to sort the processes based upon priority
  public static void priorityAlg(){
    while(pri1.peek()!=null){
      Process p = pri1.poll();
      System.out.println("Pri1 Queue: "+p.returnString());
    }
    for(Process p: pri2){
      System.out.println("Pri2 Queue: "+p.returnString());
    }
    for(Process p: pri3){
      System.out.println("Pri3 Queue: "+p.returnString());
    }
    for(Process p: pri4){
      System.out.println("Pri4 Queue: "+p.returnString());
    }
    for(Process p: pri5){
      System.out.println("Pri5 Queue: "+p.returnString());
    }
    for(Process p: pri6){
      System.out.println("Pri6 Queue: "+p.returnString());
    }
    for(Process p: pri7){
      System.out.println("Pri7 Queue: "+p.returnString());
    }
  }

  //method that utilizes the Round-Robin algorithm
  public static void rrAlg(){
    int i = 0;
    while(i<processes.size()){
      if(pri1.peek()!=null){
        Process p = pri1.poll();
        System.out.println("Pri1 Queue: "+p.returnString());
        i++;
      }
      if(pri2.peek()!=null){
        Process p = pri2.poll();
        System.out.println("Pri2 Queue: "+p.returnString());
        i++;
      }
      if(pri3.peek()!=null){
        Process p = pri3.poll();
        System.out.println("Pri3 Queue: "+p.returnString());
        i++;
      }
      if(pri4.peek()!=null){
        Process p = pri4.poll();
        System.out.println("Pri4 Queue: "+p.returnString());
        i++;
      }
      if(pri5.peek()!=null){
        Process p = pri5.poll();
        System.out.println("Pri5 Queue: "+p.returnString());
        i++;
      }
      if(pri6.peek()!=null){
        Process p = pri6.poll();
        System.out.println("Pri6 Queue: "+p.returnString());
        i++;
      }
    }
  }

  //method to order the processes
  public static void orderAlg(){
    for(Process p: processes){
      System.out.println(p.returnString());
    }
  }

  //method that prints the queues
  public static void multiQueue(){
    Scanner que = new Scanner(System.in);
    System.out.println();
  }


}

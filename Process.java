public class Process{

  public int id;
  public String pType;

  public Process(String ID, String pt){
    id = Integer.valueOf(ID);
    pType = pt;
  }



  public int getId(){
    return id;
  }

  public String getPT(){
    return pType;
  }

  public void setId(int ID){
    id = ID;
  }

  public void setPT(String pt){
    pType = pt;
  }

  public String returnString(){
    String retStr = "Process "+id+"\tProcess Type: "+pType;
    return retStr;
  }

}
